https://api.ovh.com/console/
https://eu.api.ovh.com/createToken/
https://realpython.com/async-io-python/


## TODO ##

DOMAIN/MAIL/ACOUNT
    * create/list/remove/get_quota of mail box

DOMAIN/MAIL/DELEGATION
    * really useful to implement ?
    * create/list/remove/update/task_list/etc.

DOMAIN/ZONE
    * manage task: list/accelerate/cancel/relaunch + print task creation to any script that create a task

MISC
    * add arguments wait_for_task_to_be_over, max_timeout, check_every_n_seconds to any script that create a task (mail, redirection, xdsl, etc.)
    * listing should start printing as soon as they have input, tabulate is an issue
    * manage return 0 or 1 in main + exit call when needed
    * rename alias to mail_alias
    * create global/generic logging

XDSL
    * print task information on script that create a task (like for redirection)
