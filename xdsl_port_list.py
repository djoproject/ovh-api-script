#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from tabulate import tabulate

from ovh_api_call.xdsl.port import get_every_port_mappings
from ovh_api_call.xdsl.port import get_port_mappings


async def main(endpoint: str="ovh-eu", service_name: str=None):
    client = Client(endpoint=endpoint)

    await client.init(config_file=None)
    
    table = []
    async with client._session:
        if service_name is None:
            results = await get_every_port_mappings(client)
        else:
            results = await get_port_mappings(client, service_name)

    for mapping in results:
        table.append((mapping[u'id'], mapping[u'service_name'], mapping[u'name'], mapping[u'description'], mapping[u'protocol'], mapping[u'allowedRemoteIp'], mapping[u'externalPortStart'], mapping[u'externalPortEnd'], mapping[u'internalClient'], mapping[u'internalPort'], mapping[u'taskId'], mapping[u'task_status']))

    print(tabulate(table, headers=['id', 'service_name', 'name', 'description', 'protocol', 'allowedRemoteIp', 'externalPortStart', 'externalPortEnd', 'internalClient', 'internalPort', 'taskId', 'task_status']))


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    parser.add_argument('service_name', nargs='?', type=str, default=None, help='service name')
    args = parser.parse_args()

    asyncio.run(
        main(
            endpoint=args.endpoint,
            service_name=args.service_name
        )
    )