#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import ResourceNotFoundError

from ovh_api_call.xdsl.task import archive_task


async def main(service_name: str, task_id: int, endpoint: str="ovh-eu"):
    client = Client(endpoint=endpoint)
    await client.init(config_file=None)

    async with client._session:
        try:
            await archive_task(client, service_name, task_id)
        except ResourceNotFoundError as ex:
            print(str(ex))
            return 1

        print("task {0} archived".format(task_id))
        return 0


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('service_name', type=str, help='The internal name of your XDSL offer')
    parser.add_argument('task_id', type=int, help='the task id to archive')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(asyncio.run(
        main(
            service_name=args.service_name,
            task_id=args.task_id,
            endpoint=args.endpoint,
        )
    ))