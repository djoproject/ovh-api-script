#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import BadParametersError
from asyncovh.exceptions import ResourceConflictError

from ovh_api_call.email.domain.common import get_email_domain_properties

import pprint


async def main(domain: str, endpoint: str="ovh-eu"):
    client = Client(endpoint=endpoint)

    await client.init(config_file=None)

    table = []
    async with client._session:
        infos = await get_email_domain_properties(client, domain)
        pprint.pprint(infos)
        # TODO improve printing

        # TODO get quota
        # /email/domain/{domain}/quota

        # TODO quota used (except alias)
        # /email/domain/{domain}/summary

        # TODO mailing-list particant limit
        # /email/domain/mailingListLimits

        # TODO service infos
        # /email/domain/{domain}/serviceInfos

    return 0


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', type=str, default=None, help='domain to details')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(asyncio.run(
        main(
            domain=args.domain,
            endpoint=args.endpoint
        )
    ))
