#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import ResourceConflictError
from asyncovh.exceptions import ResourceNotFoundError

from ovh_api_call.email.domain.redirection import find_redirection
from ovh_api_call.email.domain.redirection import update_redirection
from ovh_api_call.email.misc import split_mail


async def main(alias_name: str, new_target: str, old_target: str=None, endpoint: str="ovh-eu"):
    account_name, domain = split_mail(alias_name)

    client = Client(endpoint=endpoint)
    await client.init(config_file=None)
    
    async with client._session:
        redirections = await find_redirection(client, domain, alias_name, old_target)

        if len(redirections) == 0:
            print("unknown redirection")
            return 1

        if len(redirections) > 1:
            print("too many redirection")
            return 1

        try:
            task = await update_redirection(client, domain, redirections[0][u"id"], new_target)
        except ResourceNotFoundError:
            print("redirection has been removed")
            return 1
        except ResourceConflictError as ex:
            # two errors possible:
            #   * already exist
            #   * already being processed
            print(ex)
            return 1

        print("task created.  (id={0}, type={1}, action={2})".format(task[u"id"], task[u"type"], task[u"action"]))
        return 0


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('alias_name', type=str, help='sub domain to create')
    parser.add_argument('new_target', type=str, help='new target of the subdomain')
    parser.add_argument('old_target', nargs='?', type=str, help='new target of the subdomain')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(asyncio.run(
        main(
            alias_name=args.alias_name,
            new_target=args.new_target,
            old_target=args.old_target,
            endpoint=args.endpoint
        )
    ))