#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from tabulate import tabulate

from ovh_api_call.xdsl.task import get_every_tasks
from ovh_api_call.xdsl.task import get_tasks


async def main(endpoint: str="ovh-eu", service_name: str=None):
    client = Client(endpoint=endpoint)

    await client.init(config_file=None)
    
    table = []
    async with client._session:
        if service_name is None:
            results = await get_every_tasks(client)
        else:
            results = await get_tasks(client, service_name)

    for task in results:
        table.append((task[u'id'], task[u'function'], task[u'status'], task[u'todoDate'], task[u'updateDate']))

    print(tabulate(table, headers=['id', 'function', 'status', 'todoDate', 'updateDate']))
    # state observed: doing, todo
    # function observed: changeModemConfigPortMapping


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    parser.add_argument('service_name', nargs='?', type=str, default=None, help='service name')
    args = parser.parse_args()

    asyncio.run(
        main(
            endpoint=args.endpoint,
            service_name=args.service_name
        )
    )