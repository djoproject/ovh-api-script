#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import ResourceNotFoundError

from ovh_api_call.xdsl.port import delete_port_mapping
from ovh_api_call.xdsl.port import get_port_mapping


async def main(service_name: str, name: str, endpoint: str="ovh-eu"):
    client = Client(endpoint=endpoint)
    await client.init(config_file=None)

    async with client._session:
        try:
            record = await get_port_mapping(client, service_name, name)
        except ResourceNotFoundError as ex:
            print(str(ex))
            return 1

        await delete_port_mapping(client, service_name, name)
        print("removal task created")
        return 0

if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('service_name', type=str, help='The internal name of your XDSL offer')
    parser.add_argument('name', type=str, help='Name of the port mapping entry')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(asyncio.run(
        main(
            service_name=args.service_name,
            name=args.name,
            endpoint=args.endpoint,
        )
    ))