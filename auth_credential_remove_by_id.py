#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from tabulate import tabulate

from ovh_api_call.auth import remove_api_credential


async def main(credential_id: int, endpoint: str="ovh-eu"):
    client = Client(endpoint=endpoint)

    await client.init(config_file=None)
    
    table = []
    async with client._session:
        await remove_api_credential(client, credential_id)


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('credential_id', type=int, help='credential id to remove')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(
        main(
            credential_id=args.credential_id,
            endpoint=args.endpoint
        )
    )