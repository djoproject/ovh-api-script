#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import ResourceConflictError

from ovh_api_call.email.domain.redirection import create_redirection
from ovh_api_call.email.misc import split_mail


async def main(alias_name: str, target: str, endpoint: str="ovh-eu"):
    _, domain = split_mail(alias_name)

    client = Client(endpoint=endpoint)
    await client.init(config_file=None)
    
    async with client._session:
        try:
            task = await create_redirection(client, domain, alias_name, target)
            print("task created.  (id={0}, type={1}, action={2})".format(task[u"id"], task[u"type"], task[u"action"]))
            return 0
        except ResourceConflictError as ex:
            # two errors possible:
            #   * already exist
            #   * already being processed
            print(ex)
            return 1


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('alias_name', type=str, help='sub domain to create')
    parser.add_argument('target', type=str, help='target of the subdomain')
    # TODO add argument keep a local copy
    #   TODO what happen if there is no local mail box ? test it
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(asyncio.run(
        main(
            alias_name=args.alias_name,
            target=args.target,
            endpoint=args.endpoint
        )
    ))