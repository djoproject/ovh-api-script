#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
from asyncovh import Client
from asyncovh.client import ENDPOINTS

from ovh_api_call.domain.dynhost import alter_dyn_record
from ovh_api_call.domain.dynhost import create_dyn_record
from ovh_api_call.domain.dynhost import find_dyn_record
from ovh_api_call.domain.zone import refresh_zone


async def main(
    domain: str,
    sub_domain:str,
    ip: str,
    endpoint: str="ovh-eu",
    no_dns_refresh: bool=False,
    update: bool=False):

    client = Client(endpoint=endpoint)
    await client.init(config_file=None)

    async with client._session:
        record_detail = await find_dyn_record(client, domain, sub_domain)
        need_to_refresh = False

        if record_detail is None:
            await create_dyn_record(client, domain, sub_domain, ip)
            need_to_refresh = True
            message = "created"
        elif not update:
            message = "already exist"
        elif record_detail[u"ip"] == ip:
            message = "no change"
        else:
            await alter_dyn_record(client, domain, record_detail[u"id"], ip)
            need_to_refresh = True
            message = "updated"

        if need_to_refresh and not no_dns_refresh:
            await refresh_zone(client, domain)

        print(message)


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--no_refresh', default=False, action='store_true', help='do not refresh the dns zone')
    parser.add_argument('-u', '--update', default=False, action='store_true', help='update target if it has changed')
    parser.add_argument('sub_domain', type=str, help='sub domain to create')
    parser.add_argument('domain', type=str, help='dns domain where to attach the sub domain')
    parser.add_argument('ip', type=str, help='target ip of the subdomain')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(
        main(
            domain=args.domain,
            sub_domain=args.sub_domain,
            ip=args.ip,
            endpoint=args.endpoint,
            no_dns_refresh=args.no_refresh,
            update=args.update
        )
    )