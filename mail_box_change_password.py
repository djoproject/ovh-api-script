#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import ResourceNotFoundError

from ovh_api_call.email.domain.account import change_mailbox_password


async def main(domain: str, name: str, new_password: str=None, endpoint: str="ovh-eu"):
    if new_password is None:
        print("nothing to update")
        return 0

    if len(new_password) < 8:
        print("password lenght must at least be 8")
        return 1

    client = Client(endpoint=endpoint)

    await client.init(config_file=None)

    table = []
    async with client._session:
        try:
            await change_mailbox_password(client, domain, name, new_password)
        except ResourceNotFoundError as ex:
            print(ex)
            return 1

    print("updated")


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', type=str, help='dns domain where to attach the sub domain')
    parser.add_argument('name', type=str, help='account name to update')
    parser.add_argument('password', type=str, help='new password')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(
        asyncio.run(
            main(
                domain=args.domain,
                name=args.name,
                new_password=args.password,
                endpoint=args.endpoint)))
