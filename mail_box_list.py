#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from tabulate import tabulate

from ovh_api_call.email.domain.account import get_every_mail_box
from ovh_api_call.email.domain.account import get_mail_box
from ovh_api_call.email.misc import convert_bytes


async def main(endpoint: str="ovh-eu", domain: str=None):
    client = Client(endpoint=endpoint)

    await client.init(config_file=None)
    
    table = []
    async with client._session:
        if domain is None:
            mail_boxes = await get_every_mail_box(client)
        else:
            mail_boxes = await get_mail_box(client, domain)

        for mail_box in mail_boxes:
            table.append((mail_box[u"domain"], mail_box[u"accountName"], mail_box[u"description"], str(mail_box[u"isBlocked"]), convert_bytes(mail_box[u"size"]) ))

    print(tabulate(table, headers=['domain', 'accountName', 'description', 'isBlocked', 'size']))


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', nargs='?', type=str, default=None, help='dns domain where to attach the sub domain')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(main(endpoint=args.endpoint, domain=args.domain))