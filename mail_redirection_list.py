#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
import pprint
from tabulate import tabulate

from asyncovh import Client
from asyncovh.client import ENDPOINTS

from ovh_api_call.email.domain.redirection import get_every_redirections
from ovh_api_call.email.domain.redirection import get_domain_redirections


async def main(domain: str=None, endpoint: str="ovh-eu"):
    client = Client(endpoint=endpoint)

    await client.init(config_file=None)
    
    async with client._session:
        if domain is None:
            aliases = await get_every_redirections(client)
        else:
            aliases = await get_domain_redirections(client, domain)

    # sorting
    domains = {}
    for alias in aliases:
        if alias[u"domain"] not in domains:
            domains[alias[u"domain"]] = {}

        if alias[u"from"] not in domains[alias[u"domain"]]:
            domains[alias[u"domain"]][alias[u"from"]] = []

        domains[alias[u"domain"]][alias[u"from"]].append(alias)

    # building table
    table = []
    for domain in sorted(domains.keys()):
        for alias_from in sorted(domains[domain].keys()):
            for alias in sorted(domains[domain][alias_from], key=lambda a: a["to"]):
                table.append((alias[u"id"], alias[u"domain"], alias[u"from"], alias[u"to"]))

    print(tabulate(table, headers=['id', 'domain', 'from', 'to']))

if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', nargs='?', type=str, default=None, help='dns domain where to attach the sub domain')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(
        main(
            domain=args.domain,
            endpoint=args.endpoint
        )
    )