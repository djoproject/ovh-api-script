#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
from asyncovh import Client
from asyncovh.client import ENDPOINTS

from ovh_api_call.domain.zone import VALID_FIELD_TYPE
from ovh_api_call.domain.zone import delete_record
from ovh_api_call.domain.zone import find_record
from ovh_api_call.domain.zone import refresh_zone


async def main(domain: str, field_type: str, sub_domain: str=None, endpoint: str="ovh-eu", no_dns_refresh: bool=False):
    client = Client(endpoint=endpoint)
    await client.init(config_file=None)

    table = []
    async with client._session:
        record_detail = await find_record(client, domain, field_type, sub_domain)

        if record_detail is None:
            print("unknwon record")
            return 1

        await delete_record(client, domain, record_detail[u"id"])

        if not no_dns_refresh:
            await refresh_zone(client, domain)

        print("domain removed")
            
    return 0

if __name__ == "__main__":
    import argparse
    import sys

    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('-y', '--type', type=str, choices=VALID_FIELD_TYPE, default='A', help='define the target type')
    parser.add_argument('-n', '--no_refresh', default=False, action='store_true', help='do not refresh the dns zone')
    parser.add_argument('sub_domain', type=str, help='sub domain to create')
    parser.add_argument('domain', type=str, help='dns domain where is attached the sub domain')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(asyncio.run(
        main(
            domain=args.domain,
            field_type=args.type,
            sub_domain=args.sub_domain,
            endpoint=args.endpoint,
            no_dns_refresh=args.no_refresh
        )
    ))