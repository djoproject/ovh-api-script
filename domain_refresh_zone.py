#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
from asyncovh import Client
from asyncovh.client import ENDPOINTS

from ovh_api_call.domain.zone import VALID_FIELD_TYPE
from ovh_api_call.domain.zone import create_record
from ovh_api_call.domain.zone import get_domain_records
from ovh_api_call.domain.zone import refresh_zone


async def main(domain: str, endpoint: str="ovh-eu"):
    client = Client(endpoint=endpoint)
    await client.init(config_file=None)

    async with client._session:
        await refresh_zone(client, domain)

    print("refreshed")


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', type=str, help='domain name to refresh')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(
        main(
            domain=args.domain,
            endpoint=args.endpoint
        )
    )