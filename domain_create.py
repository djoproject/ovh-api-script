#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
from asyncovh import Client
from asyncovh.client import ENDPOINTS

from ovh_api_call.domain.zone import VALID_FIELD_TYPE
from ovh_api_call.domain.zone import alter_record
from ovh_api_call.domain.zone import create_record
from ovh_api_call.domain.zone import find_record
from ovh_api_call.domain.zone import refresh_zone


async def main(
    domain: str,
    field_type: str,
    target:str,
    sub_domain: str=None,
    ttl: int=60,
    endpoint: str="ovh-eu",
    no_dns_refresh: bool=False,
    update: bool=False):

    client = Client(endpoint=endpoint)
    await client.init(config_file=None)

    need_to_refresh = False
    async with client._session:
        record_detail = await find_record(client, domain, field_type, sub_domain)

        if record_detail is None:
            await create_record(client, domain, field_type, target, sub_domain, ttl)
            need_to_refresh = True
            message = "created"
        elif not update:
            message = "already exist"
        elif record_detail[u"target"] == target and record_detail[u"ttl"] == ttl:
            message = "no change"
        else:
            await alter_record(client, domain, record_detail[u"id"], target, ttl)
            need_to_refresh = True
            message = "updated"

        if need_to_refresh and not no_dns_refresh:
            await refresh_zone(client, domain)

        print(message)

if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--ttl', type=int, default=60, help='define the dns time to live')
    parser.add_argument('-y', '--type', type=str, choices=VALID_FIELD_TYPE, default='A', help='define the target type')
    parser.add_argument('-n', '--no_refresh', default=False, action='store_true', help='do not refresh the dns zone')
    parser.add_argument('-u', '--update', default=False, action='store_true', help='update target or ttl if it has changed')
    parser.add_argument('-s', '--sub_domain', type=str, help='sub domain to create')
    parser.add_argument('domain', type=str, help='dns domain where to attach the sub domain')
    parser.add_argument('target', type=str, help='target of the subdomain')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(
        main(
            domain=args.domain,
            field_type=args.type,
            target=args.target,
            sub_domain=args.sub_domain,
            ttl=args.ttl,
            endpoint=args.endpoint,
            no_dns_refresh=args.no_refresh,
            update=args.update
        )
    )