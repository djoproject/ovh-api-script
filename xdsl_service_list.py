#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS

from ovh_api_call.xdsl.misc import get_xdsl_services


async def main(endpoint: str="ovh-eu"):
    client = Client(endpoint=endpoint)

    await client.init(config_file=None)
    
    table = []
    async with client._session:
        services = await get_xdsl_services(client)

    for service in services:
        print(service)

if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(
        main(
            endpoint=args.endpoint,
        )
    )