#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS

from ovh_api_call.xdsl.port import delete_port_mapping
from ovh_api_call.xdsl.port import find_port_mapping


async def main(
    service_name: str,
    protocol: str,
    externalPortStart: int,
    internalClient: str,
    internalPort: int,
    allowedRemoteIp: str=None,
    externalPortEnd: int=None,
    endpoint: str="ovh-eu"):

    client = Client(endpoint=endpoint)
    await client.init(config_file=None)
    
    async with client._session:
        port_mapping = await find_port_mapping(client,
            service_name,
            protocol,
            externalPortStart,
            internalClient,
            internalPort,
            externalPortEnd,
            allowedRemoteIp
            )

        if port_mapping is None:
            print("unknown mapping")
            return 1

        await delete_port_mapping(client, service_name, port_mapping[u"name"])
        print("removal task created")

    return 0

if __name__ == "__main__":
    import argparse
    import sys

    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('service_name', type=str, help='The internal name of your XDSL offer')
    parser.add_argument('protocol', type=str, choices=("TCP", "UDP"), help='Protocol of the port mapping (TCP / UDP)')
    parser.add_argument('externalPortStart', type=int, help='External Port that the modem will listen on')
    parser.add_argument('internalClient', type=str, help='The IP address of the destination of the packets')
    parser.add_argument('internalPort', type=int, help='The port on the Internal Client that will get the connections')
    parser.add_argument('-r', '--allowedRemoteIp', type=str, default=None, help='An ip which will access to the defined rule')
    parser.add_argument('-x', '--externalPortEnd', type=int, default=None, help='The last port of the interval on the External Client that will get the connections')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(asyncio.run(
        main(
            service_name=args.service_name,
            protocol=args.protocol,
            externalPortStart=args.externalPortStart,
            internalClient=args.internalClient,
            internalPort=args.internalPort,
            allowedRemoteIp=args.allowedRemoteIp,
            externalPortEnd=args.externalPortEnd,
            endpoint=args.endpoint
        )
    ))