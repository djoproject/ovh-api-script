#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import ResourceConflictError
from asyncovh.exceptions import ResourceNotFoundError

from ovh_api_call.email.domain.account import update_mail_box


async def main(domain: str, name: str, new_description: str=None, new_size: int=None, endpoint: str="ovh-eu"):
    if new_description is None and new_size is None:
        print("nothing to update")
        return 0

    client = Client(endpoint=endpoint)

    await client.init(config_file=None)

    table = []
    async with client._session:
        try:
            await update_mail_box(client, domain, name, new_description, new_size)
        except (ResourceConflictError, ResourceNotFoundError) as ex:
            print(ex)
            return 1

    print("updated")


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', type=str, help='dns domain where to attach the sub domain')
    parser.add_argument('name', type=str, help='account name to update')
    parser.add_argument('-d', '--description', type=str, default=None, help='new description')
    parser.add_argument('-s', '--size', type=int, default=None, help='new size')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(
        asyncio.run(
            main(
                domain=args.domain,
                name=args.name,
                new_description=args.description,
                new_size=args.size,
                endpoint=args.endpoint)))
