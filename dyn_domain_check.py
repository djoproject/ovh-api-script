#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
from asyncovh import Client

from ovh_api_call.domain.dynhost import alter_dyn_record
from ovh_api_call.domain.dynhost import create_dyn_record
from ovh_api_call.domain.dynhost import get_domain_dyn_records
from ovh_api_call.domain.zone import refresh_zone


async def get_public_ip(client: Client):
    result = await client._session.get('https://api6.ipify.org')
    return await result.text()


async def main():
    client = Client(endpoint="ovh-eu")
    await client.init(config_file=None)

    domain = "djoproject.net"
    sub_domain = "plop"

    async with client._session:
        ip = await get_public_ip(client)
        records = await get_domain_dyn_records(client, "djoproject.net")

        for record in records:
            if sub_domain == record[u"subDomain"]:
                if ip == record[u"ip"]:
                    print("up to date")
                    break
                else:
                    await alter_dyn_record(client, domain, record[u"id"], ip)
                    await refresh_zone(client, domain)
                    print("updated")
                    break
        else:
            await create_dyn_record(client, domain, sub_domain, ip)
            await refresh_zone(client, domain)
            print("created")


if __name__ == "__main__":
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."
    asyncio.run(main())