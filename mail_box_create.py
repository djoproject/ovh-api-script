#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import BadParametersError
from asyncovh.exceptions import ResourceConflictError

from ovh_api_call.email.domain.account import create_mail_box


async def main(domain: str, name: str, password: str, description: str=None, size: int=5000000000, endpoint: str="ovh-eu"):
    if len(password) < 8:
        print("password lenght must at least be 8")
        return 1

    client = Client(endpoint=endpoint)

    await client.init(config_file=None)
    
    table = []
    async with client._session:
        try:
            await create_mail_box(client, domain, name, password, description, size)
        except (ResourceConflictError, BadParametersError) as ex:
            print(ex)
            return 1

    print("created")
    return 0


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', type=str, default=None, help='dns domain where to attach the sub domain')
    parser.add_argument('name', type=str, default=None, help='account name to create')
    parser.add_argument('password', type=str, help='account password')
    parser.add_argument('-d', '--description', type=str, default=None, help='account description')
    parser.add_argument('-s', '--size', type=int, default=5000000000, help='account size')

    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(asyncio.run(
        main(
            domain=args.domain,
            name=args.name,
            password=args.password,
            description=args.description,
            size=args.size,
            endpoint=args.endpoint
        )
    ))
