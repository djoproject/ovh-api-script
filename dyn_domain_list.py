#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
from asyncovh import Client
from asyncovh.client import ENDPOINTS
from tabulate import tabulate

from ovh_api_call.domain.dynhost import get_domain_dyn_records
from ovh_api_call.domain.dynhost import get_every_dyn_records


async def main(endpoint: str="ovh-eu", domain: str=None):
    client = Client(endpoint=endpoint)
    await client.init(config_file=None)

    table = []
    async with client._session:
        if domain is None:
            records = await get_every_dyn_records(client)
        else:
            records = await get_domain_dyn_records(client, domain)

        for record in records:
            table.append((record[u"id"], record[u"zone"], record[u"subDomain"], record[u"ip"], record[u"ttl"]))

    print(tabulate(table, headers=['id', 'zone', 'subDomain', 'ip', 'ttl']))


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', nargs='?', type=str, help='dns domain where to attach the sub domain')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(
        main(
            domain=args.domain,
            endpoint=args.endpoint
        )
    )