#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from tabulate import tabulate

from ovh_api_call.auth import get_api_credentials


async def main(endpoint: str="ovh-eu"):
    client = Client(endpoint=endpoint)

    await client.init(config_file=None)
    
    table = []
    async with client._session:
        result = await get_api_credentials(client)

    for credential in result:
        rules = []
        for rule in credential[u'rules']:
            rules.append("{0} {1}".format(rule[u'method'], rule[u'path']))
        rules.append("---")

        table.append((credential[u'credentialId'], str(credential[u'applicationId']), credential[u'applicationKey'], credential[u'name'], credential[u'description'], "{0}\n{1}".format(credential[u'creation'], credential[u'expiration']), credential[u'lastUse'], credential[u'status'], credential[u'ovhSupport'], "\n".join(rules)))

    print(tabulate(table, headers=['credentialId', 'applicationId', 'applicationKey', 'name', 'description', 'creation/expiration', 'lastUse', 'status', 'ovhSupport', 'rules']))


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(
        main(
            endpoint=args.endpoint
        )
    )