#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from tabulate import tabulate

from ovh_api_call.email.domain.account import get_tasks
from ovh_api_call.email.domain.account import get_domain_tasks


async def main(endpoint: str="ovh-eu", domain: str=None):
    client = Client(endpoint=endpoint)

    await client.init(config_file=None)
    
    table = []
    async with client._session:
        if domain is None:
            tasks = await get_tasks(client)
        else:
            tasks = await get_domain_tasks(client, domain)

        import pprint
        pprint.pprint(tasks)

        for task in tasks:
            table.append( (task[u"id"], task[u"domain"], task[u"name"], task[u"action"], task[u"date"],) )

    print(tabulate(table, headers=['id', 'domain', 'name', 'action', 'date']))


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', nargs='?', type=str, default=None, help='dns domain where to attach the sub domain')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(main(endpoint=args.endpoint, domain=args.domain))