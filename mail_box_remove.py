#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import ResourceNotFoundError

from ovh_api_call.email.domain.account import remove_mail_box


async def main(domain: str, name: str, endpoint: str="ovh-eu"):
    client = Client(endpoint=endpoint)

    await client.init(config_file=None)

    async with client._session:
        try:
            await remove_mail_box(client, domain, name)
        except ResourceNotFoundError as ex:
            print(ex)
            return 1

    print("removed")
    return 0


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', type=str, default=None, help='dns domain where to attach the sub domain')
    parser.add_argument('name', type=str, default=None, help='account name to remove')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(asyncio.run(main(domain=args.domain, name=args.name, endpoint=args.endpoint)))
