#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from tabulate import tabulate

from ovh_api_call.auth import get_api_applications


async def main(endpoint: str="ovh-eu"):
    client = Client(endpoint=endpoint)

    await client.init(config_file=None)
    
    table = []
    async with client._session:
        result = await get_api_applications(client)

    for app in result:
        table.append((app['applicationId'], app['applicationKey'], app['description'], app['name'], app['status'], app['status']))

    print(tabulate(table, headers=['applicationId', 'applicationKey', 'description', 'name', 'status', 'status']))


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(
        main(
            endpoint=args.endpoint
        )
    )