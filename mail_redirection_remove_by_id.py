#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import ResourceNotFoundError
from asyncovh.exceptions import ResourceConflictError

from ovh_api_call.email.domain.redirection import remove_redirection


async def main(alias_id: str, domain: str, endpoint: str="ovh-eu"):
    client = Client(endpoint=endpoint)
    await client.init(config_file=None)
    
    async with client._session:
        try:
            task = await remove_redirection(client, domain, alias_id)
        except ResourceNotFoundError:
            print("unknown redirection")
            return 1
        except ResourceConflictError:
            print("a task is already")
            return 1

        print("task created.  (id={0}, type={1}, action={2})".format(task[u"id"], task[u"type"], task[u"action"]))
        return 0


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', type=str, help='dns domain where to attach the sub domain')
    parser.add_argument('alias_id', type=int, help='alias id to remove')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(asyncio.run(
        main(
            alias_id=args.alias_id,
            domain=args.domain,
            endpoint=args.endpoint
        )
    ))