#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import ResourceNotFoundError
from asyncovh.exceptions import ResourceConflictError

from ovh_api_call.email.domain.redirection import remove_redirection
from ovh_api_call.email.domain.redirection import find_redirection
from ovh_api_call.email.misc import split_mail

async def main(mail_from: str, target: str=None, endpoint: str="ovh-eu"):
    _, domain = split_mail(mail_from)

    client = Client(endpoint=endpoint)
    await client.init(config_file=None)

    async with client._session:
        results = await find_redirection(client, domain, mail_from, target)

        if len(results) == 0:
            print("not found")
            return 0

        tasks = []
        for redirection_details in results:
            tasks.append(remove_redirection(client, domain, redirection_details[u"id"]))

        return_code = 0
        for f in asyncio.as_completed(tasks):
            try:
                task = await f
                print("task created.  (id={0}, type={1}, action={2})".format(task[u"id"], task[u"type"], task[u"action"]))
            except ResourceNotFoundError:
                print("unknown redirection")
                return_code = 1
            except ResourceConflictError:
                print("a task is already")
                return_code = 1

        return return_code


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('alias_name', type=str, help='sub domain to create')
    parser.add_argument('target', nargs='?', type=str, help='new target of the subdomain')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    exit(asyncio.run(
        main(
            mail_from=args.alias_name,
            target=args.target,
            endpoint=args.endpoint
        )
    ))