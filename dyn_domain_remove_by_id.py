#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
from asyncovh import Client
from asyncovh.client import ENDPOINTS
from asyncovh.exceptions import ResourceNotFoundError

from ovh_api_call.domain.dynhost import delete_dyn_record
from ovh_api_call.domain.dynhost import get_domain_dyn_record
from ovh_api_call.domain.zone import refresh_zone


async def main(domain: str, record_id: int, endpoint: str="ovh-eu", no_dns_refresh: bool=False):
    client = Client(endpoint=endpoint)
    await client.init(config_file=None)

    async with client._session:
        try:
            record = await get_domain_dyn_record(client, domain, record_id)
        except ResourceNotFoundError as ex:
            print(str(ex))
            return 1

        await delete_dyn_record(client, domain, record_id)

        if not no_dns_refresh:
            await refresh_zone(client, domain)

        print("removed")

if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--no_refresh', default=False, action='store_true', help='do not refresh the dns zone')
    parser.add_argument('sub_domain_id', type=int, help='sub domain id to remove')
    parser.add_argument('domain', type=str, help='dns domain where to attach the sub domain')
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    args = parser.parse_args()

    asyncio.run(
        main(
            domain=args.domain,
            record_id=args.sub_domain_id,
            endpoint=args.endpoint,
            no_dns_refresh=args.no_refresh
        )
    )