#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pprint import pprint

from configparser import ConfigParser
from os import W_OK
from os import access
from os.path import exists
from os.path import expanduser

import asyncio

from asyncovh import Client
from asyncovh.config import CONFIG_PATH
from asyncovh.client import ENDPOINTS
from tabulate import tabulate

from ovh_api_call.auth import RULES_ALL_ACCESS
from ovh_api_call.auth import create_creds
from ovh_api_call.me import me


async def main(
    file_path: str,
    application_key: str=None,
    application_secret: str=None,
    endpoint_default: bool=True,
    rules: list=None,
    endpoint: str="ovh-eu"):

    if file_path not in CONFIG_PATH:
        print("path is not one of the default path defined in python-asyncovh")
        exit(1)

    file_exists = exists(file_path)

    if file_exists and not access(file_path, W_OK):
        print("config file is not writtable, credentials not renewed")
        exit(1)

    if rules is not None:
        parsed_rules = []
        for raw_rule in rules:
            split_rule = raw_rule.split()

            if len(split_rule) != 2:
                print("invalid rule (too many tokens): {0}".format(raw_rule))
                exit(1)

            if split_rule[0].upper() not in ("GET", "PUT", "POST", "DELETE",):
                print("invalid rule (does not start with a http method): {0}".format(raw_rule))
                exit(1)

            parsed_rules.append((split_rule[0].upper(), split_rule[1]))
    else:
        parsed_rules = RULES_ALL_ACCESS

    client = Client(endpoint=endpoint)
    await client.init(config_file=None)

    table = []
    async with client._session:
        result = await create_creds(client, parsed_rules)

        pprint(result)

        print("Please visit {0} to authenticate".format(result['validationUrl']))
        input("and press Enter to continue...")

        # make a test
        pprint(await me(client))

        config_data = ConfigParser()

        if file_exists:
            config_data.read(file_path)

        if endpoint_default:
            config_data.set('default', 'endpoint', endpoint)

        if application_key is not None:
            config_data.set(endpoint, 'application_key', application_key)

        if application_secret is not None:
            config_data.set(endpoint, 'application_secret', application_secret)

        config_data.set(endpoint, 'consumer_key', result[u'consumerKey'])

        with open(file_path, 'w') as configfile:
            config_data.write(configfile)


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--endpoint', type=str, choices=ENDPOINTS.keys(), default='ovh-eu', help='endpoint target')
    parser.add_argument('-f', '--file', type=str, default=expanduser("~/.ovhrc"), help='target config file')
    parser.add_argument('-k', '--application_key', type=str, help='application key')
    parser.add_argument('-s', '--application_secret', type=str, help='application secret')
    parser.add_argument('-d', '--set_endpoint_default', action='store_true', default=False, help='set the endpoint as default')
    parser.add_argument('-r', '--rule', type=str, action='append', help="add api call rule(s)")

    args = parser.parse_args()

    asyncio.run(
        main(
            file_path=args.file,
            application_key=args.application_key,
            application_secret=args.application_secret,
            endpoint_default=args.set_endpoint_default,
            rules=args.rule,
            endpoint=args.endpoint
        )
    )
