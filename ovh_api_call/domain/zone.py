#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client


VALID_FIELD_TYPE = set([
    "A",
    "AAAA",
    "CAA",
    "CNAM",
    "DKIM",
    "DMARK",
    "LOC",
    "MX",
    "NAPTR",
    "NS",
    "PTR",
    "SPF",
    "SRV",
    "SSHFP",
    "TLSA",
    "TXT"
])


async def get_zone_domains(client: Client):
    return await client.get(_target="/domain/zone")


async def refresh_zone(client: Client, domain: str):
    target = "/domain/zone/{0}/refresh".format(domain)
    await client.post(_target=target)


async def get_domain_record(client: Client, domain: str, record_id: int):
    return await client.get(_target="/domain/zone/{0}/record/{1}".format(domain, record_id))


async def get_domain_records(client: Client, domain: str):
    records_ids = await client.get(_target="/domain/zone/{0}/record".format(domain))

    tasks = []
    for record_id in records_ids:
        tasks.append(get_domain_record(client, domain, record_id))

    return await asyncio.gather(*tasks)


async def get_every_records(client: Client):
    domains = await get_zone_domains(client)

    tasks = []
    for domain in domains:
        tasks.append(get_domain_records(client, domain))

    results = await asyncio.gather(*tasks)

    final_result = []
    for result in results:
        final_result.extend(result)

    return final_result


async def find_record(client: Client, domain: str, field_type: str, sub_domain: str):
    records_ids = await client.get(_target="/domain/zone/{0}/record".format(domain))

    tasks = []
    for records_id in records_ids:
        tasks.append(asyncio.Task(get_domain_record(client, domain, records_id)))

    for f in asyncio.as_completed(tasks):
        record_detail = await f

        if record_detail[u"fieldType"] == field_type and record_detail[u"subDomain"] == sub_domain:
            break
    else:
        return None  # not found

    # no need to wait other tasks
    for t in tasks:
        t.cancel()

    return record_detail


async def alter_record(client: Client, domain: str, record_id: int, new_target: str=None, new_ttl: int=None):
    target = "/domain/zone/{0}/record/{1}".format(domain, record_id)
    data = {}

    if new_target is not None:
        data[u"target"] = new_target

    if new_ttl is not None:
        data[u"ttl"] = new_ttl

    if len(data) == 0:
        return

    await client.put(_target=target, **data)


async def create_record(client: Client, domain: str, field_type: str, target: str, sub_domain: str=None, ttl: int=60):
    if field_type not in VALID_FIELD_TYPE:
        raise Exception("Unknown field type {0}".format(field_type))

    target_url = "/domain/zone/{0}/record".format(domain)
    data = {
        u"fieldType": field_type,
        u"target": target,
        u"ttl": ttl
    }

    if sub_domain is not None:
        data[u"subDomain"] = sub_domain

    await client.post(_target=target_url, **data)


async def delete_record(client: Client, domain: str, record_id: int):
    target = "/domain/zone/{0}/record/{1}".format(domain, record_id)
    await client.delete(_target=target)