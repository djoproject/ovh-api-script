#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client
from ovh_api_call.domain.zone import get_zone_domains


async def get_domain_dyn_record(client: Client, domain: str, record_id: int):
    return await client.get(_target="/domain/zone/{0}/dynHost/record/{1}".format(domain, record_id))


async def get_domain_dyn_records(client: Client, domain: str):
    records_ids = await client.get(_target="/domain/zone/{0}/dynHost/record".format(domain))

    tasks = []
    for record_id in records_ids:
        tasks.append(get_domain_dyn_record(client, domain, record_id))

    return await asyncio.gather(*tasks)


async def get_every_dyn_records(client: Client):
    domains = await get_zone_domains(client)

    tasks = []
    for domain in domains:
        tasks.append(get_domain_dyn_records(client, domain))

    results = await asyncio.gather(*tasks)

    final_result = []
    for result in results:
        final_result.extend(result)

    return final_result


async def find_dyn_record(client: Client, domain: str, sub_domain: str):
    records_ids = await client.get(_target="/domain/zone/{0}/dynHost/record".format(domain))

    tasks = []
    for records_id in records_ids:
        tasks.append(asyncio.Task(get_domain_dyn_record(client, domain, records_id)))

    for f in asyncio.as_completed(tasks):
        record_detail = await f

        if record_detail[u"subDomain"] == sub_domain:
            break
    else:
        return None  # not found

    # no need to wait other tasks
    for t in tasks:
        t.cancel()

    return record_detail


async def alter_dyn_record(client: Client, domain: str, record_id: int, new_ip: str):  # TODO to test
    target = "/domain/zone/{0}/dynHost/record/{1}".format(domain, record_id)
    data = {
        u"ip": new_ip
    }
    await client.put(_target=target, **data)


async def create_dyn_record(client: Client, domain: str, zone_name: str, ip:str):
    target = "/domain/zone/{0}/dynHost/record".format(domain)
    data = {
        u"subDomain": zone_name,
        u"ip": ip
    }
    await client.post(_target=target, **data)


async def delete_dyn_record(client: Client, domain: str, record_id: int):
    target = "/domain/zone/{0}/dynHost/record/{1}".format(domain, record_id)
    await client.delete(_target=target)