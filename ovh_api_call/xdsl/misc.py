#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client

async def get_xdsl_services(client: Client):
    return await client.get(_target="/xdsl")


async def get_incidents(client: Client):
    incidents = await client.get(_target="/xdsl/incidents")

    tasks = []
    for index in range(0, 10):
        incident_id = incidents[index]
        tasks.append(client.get(_target="/xdsl/incidents/{0}".format(incident_id)))

    return await asyncio.gather(*tasks)


async def get_connected_devices(client: Client, xdsl_service: str):
    return await client.get(_target="/xdsl/{0}/modem/connectedDevices".format(xdsl_service))