#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
from ipaddress import ip_address
from ipaddress import IPv4Address

from asyncovh import Client
from asyncovh.exceptions import ResourceNotFoundError

from ovh_api_call.xdsl.misc import get_xdsl_services
from ovh_api_call.xdsl.task import get_task


async def get_port_mapping(client: Client, service_name: str, mapping_name: str):
    result = await client.get(_target="/xdsl/{0}/modem/portMappings/{1}".format(service_name, mapping_name))
    result[u"service_name"] = service_name

    task_id = result[u"taskId"]

    try:
        task_details = await get_task(client, service_name, task_id)
        result[u"task_status"] = task_details[u"status"]
    except ResourceNotFoundError:
        result[u"task_status"] = "ARCHIVED"

    return result


async def get_port_mappings(client: Client, service_name: str):
    mapping_names = await client.get(_target="/xdsl/{0}/modem/portMappings".format(service_name))

    tasks = []
    for mapping_name in mapping_names:
        tasks.append(get_port_mapping(client, service_name, mapping_name))

    return await asyncio.gather(*tasks)


async def get_every_port_mappings(client: Client):
    service_names = await get_xdsl_services(client)

    tasks = []
    for service_name in service_names:
        tasks.append(get_port_mappings(client, service_name))

    results = await asyncio.gather(*tasks)

    final_result = []

    for r in results:
        final_result.extend(r)

    return final_result


async def find_port_mapping(
    client: Client,
    service_name: str,
    protocol: str,
    externalPortStart: int,
    internalClient: str,
    internalPort: int,
    externalPortEnd: int=None,
    allowedRemoteIp: str=None):

    if externalPortEnd is None:
        externalPortEnd = externalPortStart

    mapping_names = await client.get(_target="/xdsl/{0}/modem/portMappings".format(service_name))

    tasks = []
    for mapping_name in mapping_names:
        tasks.append(asyncio.create_task(get_port_mapping(client, service_name, mapping_name)))

    for f in asyncio.as_completed(tasks):
        mapping_detail = await f

        if protocol != mapping_detail[u"protocol"]:
            continue

        if externalPortStart != mapping_detail[u"externalPortStart"]:
            continue

        if internalClient != mapping_detail[u"internalClient"]:
            continue

        if internalPort != mapping_detail[u"internalPort"]:
            continue

        if externalPortEnd != mapping_detail[u"externalPortEnd"]:
            continue

        if allowedRemoteIp != mapping_detail[u"allowedRemoteIp"]:
            continue

        break
    else:
        return None  # not found

    # no need to wait other tasks
    for t in tasks:
        t.cancel()

    return mapping_detail


async def add_port_mapping(
    client: Client,
    service_name: str,
    name: str,
    protocol: str,
    externalPortStart: int,
    internalClient: str,
    internalPort: int,
    externalPortEnd: int=None,
    allowedRemoteIp: str=None,
    description: str=None
):
    kwargs = {}

    if protocol not in ("TCP", "UDP"):
        raise ValueError("protocol value must be TCP or UDP")

    if externalPortStart < 0 or externalPortStart > 65535:
        raise ValueError("externalPortStart must be between 0 and 65535")

#   TODO FIXME, forbiden port into externqlPort range, not internal
#     if internalPort in (22,):
#         raise ValueError("internalPort {0} is reserved by OVH system".format(internalPort))

    if internalPort < 0 or internalPort > 65535:
        raise ValueError("internalPort must be between 0 and 65535")

    if externalPortEnd is not None:
        if externalPortEnd < 0 or externalPortEnd > 65535:
            raise ValueError("externalPortEnd must be between 0 and 65535")

        kwargs[u"externalPortEnd"] = externalPortEnd

    try:
        addr = ip_address(internalClient)

        if not isinstance(addr, IPv4Address):
            raise ValueError("only ipv4 address is accepted for internalClient")

        internalClient = str(addr)

    except ValueError:
        raise ValueError("internalClient must be a valid ipv4 address")

    if allowedRemoteIp is not None:
        try:
            addr = ip_address(allowedRemoteIp)

            if not isinstance(addr, IPv4Address):
                raise ValueError("only ipv4 address is accepted for allowedRemoteIp")

            allowedRemoteIp = str(addr)

        except ValueError:
            raise ValueError("allowedRemoteIp must be a valid ipv4 address")

        kwargs[u"allowedRemoteIp"] = allowedRemoteIp

    kwargs[u"externalPortStart"] = externalPortStart #// External Port that the modem will listen on (type: long)
    kwargs[u"internalClient"] = internalClient #// The IP address of the destination of the packets (type: ip)
    kwargs[u"internalPort"] = internalPort #// The port on the Internal Client that will get the connections (type: long)
    kwargs[u"name"] = name #// Name of the port mapping entry (type: string)
    kwargs[u"protocol"] = protocol #// Protocol of the port mapping (TCP / UDP) (type: xdsl.xdslModemConfig.ProtocolTypeEnum)

    if description is not None:
        kwargs[u"description"] = description, #// Description of the Port Mapping (type: string)

    await client.post(_target="/xdsl/{0}/modem/portMappings".format(service_name), **kwargs)


async def delete_port_mapping(client: Client, service_name: str, mapping_name: str):
    target = "/xdsl/{0}/modem/portMappings/{1}".format(service_name, mapping_name)
    await client.delete(_target=target)