#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client

from ovh_api_call.xdsl.misc import get_xdsl_services


async def get_every_tasks(client: Client):
    service_names = await get_xdsl_services(client)

    tasks = []
    for service_name in service_names:
        tasks.append(get_tasks(client, service_name))

    results = await asyncio.gather(*tasks)

    final_result = []

    for r in results:
        final_result.extend(r)

    return final_result

async def get_tasks(client: Client, service_name: str):
    task_ids = await client.get(_target="/xdsl/{0}/tasks".format(service_name))

    tasks = []
    for task_id in task_ids:
        tasks.append(get_task(client, service_name, task_id))

    return await asyncio.gather(*tasks)


async def get_task(client: Client, service_name: str, task_id: int):
    return await client.get(_target="/xdsl/{0}/tasks/{1}".format(service_name, task_id))


async def archive_task(client: Client, service_name: str, task_id: int):
    await client.post(_target="/xdsl/{0}/tasks/{1}/archive".format(service_name, task_id))
