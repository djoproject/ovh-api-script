#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import datetime

import asyncio

from asyncovh import Client

RULES_ALL_ACCESS = [
    ("GET", "*",),
    ("POST", "*",),
    ("DELETE", "*",),
    ("PUT", "*",),
]


async def create_creds(client: Client, rules: list=RULES_ALL_ACCESS):
    cred_request = client.new_consumer_key_request()

    for rule in rules:
        cred_request.add_rule(*rule)

    return await cred_request.request()


async def get_creds(client: Client):
    return await client.get(_target="/auth/currentCredential")


async def get_api_credential(client: Client, credential_id: int):
    task1 = asyncio.create_task(client.get(_target="/me/api/credential/{0}".format(credential_id)))
    task2 = asyncio.create_task(client.get(_target="/me/api/credential/{0}/application".format(credential_id)))

    result = await task1
    result.update(await task2)

    return result

async def get_api_credentials(client: Client):
    credential_ids = await client.get(_target="/me/api/credential")

    tasks = []
    for credential_id in credential_ids:
        tasks.append(get_api_credential(client, credential_id))

    return await asyncio.gather(*tasks)


async def remove_api_credential(client: Client, credential_id: int):
    await client.delete(_target="/me/api/credential/{0}".format(credential_id))


async def remove_api_credentials_if_expired(client: Client):
    credential_ids = await client.get(_target="/me/api/credential")

    tasks = []
    for credential_id in credential_ids:
        tasks.append(remove_api_credential_if_expired(client, credential_id))

    return await asyncio.gather(*tasks)


async def remove_api_credential_if_expired(client: Client, credential_id: int):
    credential_detail = await get_api_credential(client, credential_id)

    expired_datetime = datetime.datetime.strptime(credential_detail[u"expiration"], "%Y-%m-%dT%H:%M:%S%z")

    if expired_datetime > datetime.datetime.now(datetime.timezone.utc):
        return

    await client.delete(_target="/me/api/credential/{0}".format(credential_id))


async def get_api_application(client: Client, application_id: int):
    return await client.get(_target="/me/api/application/{0}".format(application_id))


async def get_api_applications(client: Client):
    application_ids = await client.get(_target="/me/api/application")

    tasks = []
    for application_id in application_ids:
        tasks.append(get_api_application(client, application_id))

    return await asyncio.gather(*tasks)


async def remove_api_application(client: Client, app_id: int):
    await client.delete(_target="/me/api/application/{0}".format(app_id))
