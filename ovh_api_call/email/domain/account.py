#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client

from ovh_api_call.email.domain.common import get_mail_domains


async def get_mail_box_usage(client: Client, domain: str, accountName: str):
    # BUGGY DO NOT USE
    # TODO what is buggy ? need more details
    await client.post(_target="/email/domain/{0}/account/{1}/updateUsage".format(domain, accountName))
    return await client.get(_target="/email/domain/{0}/account/{1}/usage".format(domain, accountName))


async def create_mail_box(client: Client, domain: str, accountName: str, password: str, description: str=None, size: int=5000000000):
    kwargs = {
        u"_target": "/email/domain/{0}/account".format(domain),
        u"accountName": accountName,
        u"password": password,
        u"size": size
    }

    if description is not None:
        kwargs[u"description"] = description

    return await client.post(**kwargs)


async def remove_mail_box(client: Client, domain: str, accountName: str):
    return await client.delete(_target="/email/domain/{0}/account/{1}".format(domain, accountName))


async def update_mail_box(client: Client, domain: str, accountName: str, new_description: str=None, new_size: int=None):
    if new_description is None and new_size is None:
        # nothing to do
        return

    kwargs = {
        u"_target": "/email/domain/{0}/account/{1}".format(domain, accountName),
    }

    if new_description is not None:
        kwargs[u"description"] = new_description

    if new_size is not None:
        kwargs[u"size"] = new_size

    # looks like it does not return a task object ?? to check, maybe the swagger is just wrong
    return await client.put(**kwargs)


async def change_mailbox_password(client: Client, domain: str, accountName: str, new_password: str):
    kwargs = {
        u"_target": "/email/domain/{0}/account/{1}/changePassword".format(domain, accountName),
        u"password": new_password
    }

    await client.post(**kwargs)


async def get_every_mail_box(client: Client):
    domains = await get_mail_domains(client)

    tasks = []
    for domain in domains:
        tasks.append(get_mail_box(client, domain))

    results = await asyncio.gather(*tasks)

    final_result = []
    for r in results:
        final_result.extend(r)

    return final_result


async def get_mail_box(client: Client, domain: str):
    data = await client.get(_target="/email/domain/{0}/account".format(domain))

    tasks = []
    for account_name in data:
        tasks.append(get_mailbox_details(client, domain, account_name))

    return await asyncio.gather(*tasks)


async def get_mailbox_details(client: Client, domain: str, account_name: str):
    return await client.get(_target="/email/domain/{0}/account/{1}".format(domain, account_name))


async def get_tasks(client: Client):
    domains = await get_mail_domains(client)

    tasks = []
    for domain in domains:
        tasks.append(get_domain_tasks(client, domain))

    results = await asyncio.gather(*tasks)

    final_result = []
    for r in results:
        final_result.extend(r)

    return final_result


async def get_domain_tasks(client: Client, domain: str):
    task_ids = await client.get(_target="/email/domain/{0}/task/account".format(domain))

    tasks = []
    for task_id in task_ids:
        tasks.append(get_task(client, domain, task_id))

    return await asyncio.gather(*tasks)

async def get_task(client: Client, domain: str, task_id: int):
    return await client.get(_target="/email/domain/{0}/task/account/{1}".format(domain, task_id))
