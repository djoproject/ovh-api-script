#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### IMPORTANT: the unicity constraint is mail_from+mail_to, not mail_from alone !!!

import asyncio

from asyncovh import Client
from asyncovh.exceptions import ResourceNotFoundError

from ovh_api_call.email.domain.common import get_mail_domains


async def create_redirection(client: Client, domain: str, mail_from: str, mail_to: str, local_copy: bool=False) -> dict:
    """
        This function will create one tasks:
            * action=add, type=forward
    """
    kwargs = {
        u"_target": "/email/domain/{0}/redirection".format(domain),
        u"from": mail_from,
        u"to": mail_to,
        u"localCopy": local_copy
    }

    return await client.post(**kwargs)


async def get_every_redirections(client: Client):
    domains = await get_mail_domains(client)

    tasks = []
    for domain in domains:
        tasks.append(get_domain_redirections(client, domain))

    final_results = []
    for t in asyncio.as_completed(tasks):
        try:
            r = await t
        except ResourceNotFoundError:
            continue

        final_results.extend(r)

    return final_results


async def get_domain_redirections(client: Client, domain: str):
    data = await client.get(_target="/email/domain/{0}/redirection".format(domain))

    tasks = []
    for redirection_id in data:
        tasks.append(get_redirections_details(client, domain, redirection_id))

    final_results = []
    for t in asyncio.as_completed(tasks):
        try:
            r = await t
        except ResourceNotFoundError:
            continue

        final_results.append(r)

    return final_results


async def get_redirections_details(client: Client, domain: str, redirection_id: str):
    details = await client.get(_target="/email/domain/{0}/redirection/{1}".format(domain, redirection_id))
    details[u"domain"] = domain
    return details


async def remove_redirection(client: Client, domain: str, redirection_id: str):
    """
        This function will create one tasks:
            * action=delete, type=alias
    """
    return await client.delete(_target="/email/domain/{0}/redirection/{1}".format(domain, redirection_id))


async def find_redirection(client: Client, domain: str, mail_from: str, mail_to: str=None):
    target = "/email/domain/{0}/redirection?from={1}".format(domain, mail_from)

    if mail_to is not None:
        target += "&to={0}".format(mail_to)

    data = await client.get(_target=target)

    tasks = []
    for redirection_id in data:
        tasks.append(get_redirections_details(client, domain, redirection_id))

    final_results = []
    for t in asyncio.as_completed(tasks):
        try:
            r = await t
        except ResourceNotFoundError:
            continue

        final_results.append(r)

    return final_results


async def update_redirection(client: Client, domain: str, redirection_id: int, new_target: str):
    """
        This function will create two tasks:
            * action=delete, type=forward
            * action=add, type=alias

        And yes, it is two different types, this is not a typo mistake.  
    """
    kwargs = {
        u"_target": "/email/domain/{0}/redirection/{1}/changeRedirection".format(domain, redirection_id),
        u"to": new_target
    }

    return await client.post(**kwargs)


async def get_every_tasks(client: Client):
    domains = await get_mail_domains(client)

    tasks = []
    for domain in domains:
        tasks.append(get_domain_tasks(client, domain))

    final_results = []
    for t in asyncio.as_completed(tasks):
        try:
            r = await t
        except ResourceNotFoundError:
            continue

        final_results.extend(r)

    return final_results


async def get_domain_tasks(client: Client, domain: str, account_name: str=None):
    kwargs = {
        u"_target": "/email/domain/{0}/task/redirection".format(domain)
    }

    if account_name is not None:  # TODO won't work, this is not a POST call, but a GET call
        kwargs[u"account"] = account_name

    task_ids = await client.get(**kwargs)

    tasks = []
    for task_id in task_ids:
        tasks.append(get_task(client, domain, task_id))

    final_results = []
    for t in asyncio.as_completed(tasks):
        try:
            r = await t
        except ResourceNotFoundError:
            continue

        final_results.append(r)

    return final_results

async def get_task(client: Client, domain: str, task_id: int):
    return await client.get(_target="/email/domain/{0}/task/redirection/{1}".format(domain, task_id))