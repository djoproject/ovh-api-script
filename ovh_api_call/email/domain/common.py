#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client


async def get_mail_domains(client: Client):
    return await client.get(_target="/email/domain")


async def get_email_domain_properties(client: Client, domain: str):
    return await client.get(_target="/email/domain/{0}".format(domain))

# TODO
### QUOTA/USAGE
# GET /email/domain/{domain}/summary
# GET/PUT /email/domain/{domain}/serviceInfos
# GET /email/domain/{domain}/quota

### DNS details
# GET /email/domain/{domain}/recommendedDNSRecords
# GET /email/domain/{domain}/dnsMXRecords
