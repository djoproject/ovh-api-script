#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client

from ovh_api_call.email.domain.common import get_mail_domains


# GET /email/domain/{domain}/responder Get responders
# GET /email/domain/{domain}/responder/{account} Get this object properties

# POST /email/domain/{domain}/responder Create new responder in server

# PUT /email/domain/{domain}/responder/{account} Alter this object properties

# DELETE /email/domain/{domain}/responder/{account} Delete an existing responder in server

# GET /email/domain/{domain}/task/responder Get responder tasks
# GET /email/domain/{domain}/task/responder/{id} Get this object properties