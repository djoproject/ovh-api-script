#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def split_mail(mail_address):
    first_aroba = mail_address.find("@")

    if first_aroba == -1 or first_aroba == 0 or first_aroba == len(mail_address) - 1:
        raise Exception("Invalid mail address")

    second_aroba = mail_address.find("@", first_aroba+1)

    if second_aroba != -1:
        raise Exception("Invalid mail address")

    return mail_address[:first_aroba], mail_address[first_aroba+1:]


def convert_bytes(num):
    """
    this function will convert bytes to MB.... GB... etc
    """
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB']:
        if num < 1000.0:
            return "%3.1f %s" % (num, x)
        num /= 1000.0