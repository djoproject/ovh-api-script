#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio

from asyncovh import Client

async def me(client: Client):
    return await client.get(_target="/me")